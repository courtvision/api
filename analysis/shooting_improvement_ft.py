import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nba_data_api.settings')

import django

django.setup()

from app.models.PlayByPlay import Event, Possession
from app.models.Player import PlayerSeason
from django.db.models import Count
import pandas as pd

ft_counts = {
    x['player1']: x['total'] for x in
    Event.objects
        .filter(event_type=3,
                player1__season__season_type='Regular Season')
        .values('player1')
        .annotate(total=Count('player1'))
        .order_by('-total')
    if x['total'] > 250
}

year_to_year = []
for player_season_id in ft_counts:
    player_season = PlayerSeason.objects.get(pk=player_season_id)
    try:
        previous_player_season = PlayerSeason.objects.get(person_id=player_season.person.id,
                                                          season__year=player_season.season.year - 1,
                                                          season__season_type='Regular Season')
    except PlayerSeason.DoesNotExist:
        continue

    try:
        ft_counts[previous_player_season.id]
    except KeyError:
        continue

    year1_fta = Event.objects.filter(event_type=3, player1=previous_player_season)
    year1_ftm = year1_fta.exclude(description__contains="Missed").exclude(description__contains="MISS")
    year2_fta = Event.objects.filter(event_type=3, player1=player_season)
    year2_ftm = year2_fta.exclude(description__contains="Missed").exclude(description__contains="MISS")

    entry = {
        'id': player_season.person.id,
        'name': player_season.person.name,
        'year1': previous_player_season.season.year,
        'year2': player_season.season.year,
        'year1_fta': len(year1_fta),
        'year1_ft_pct': len(year1_ftm) / len(year1_fta) * 100,
        'year2_fta': len(year2_fta),
        'year2_ft_pct': len(year2_ftm) / len(year2_fta) * 100
    }
    entry['ft_pct_diff'] = entry['year2_ft_pct'] - entry['year1_ft_pct']
    year_to_year.append(entry)

year_to_year = sorted(year_to_year, key=lambda k: k['ft_pct_diff'], reverse=True)
df = pd.DataFrame(year_to_year)
df.to_csv('shooting_improvement_ft.csv')
