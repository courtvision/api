import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nba_data_api.settings')

import django

django.setup()

from app.models.PlayByPlay import Event
from app.models.Player import PlayerSeason
from django.db.models import Count

import pandas as pd

jump_shots_3pt_counts = {x['player1']: x['total'] for x in Event.objects
    .filter(possession__game__season__year=2019, event_type=1, zone__in=['Arc3', 'Corner3'])
    .values('player1')
    .annotate(total=Count('player1'))
    .order_by('-total')}

jump_shots_2pt_counts = {x['player1']: x['total'] for x in Event.objects \
    .filter(possession__game__season__year=2019, event_type=1, zone__in=['LongMidRange']) \
    .values('player1') \
    .annotate(total=Count('player1')) \
    .order_by('-total')}

filtered_3pt = [k for k, v in jump_shots_3pt_counts.items() if v > 100]
filtered_2pt = [k for k, v in jump_shots_2pt_counts.items() if v > 10]
filtered_players = [x for x in filtered_3pt if x in filtered_2pt]

assisted_percentages = []
for player in filtered_players:
    player_2pt_shots = Event.objects.filter(possession__game__season__year=2019, event_type=1,
                                            zone__in=['LongMidRange'], player1=player)
    player_2pt_shots_assisted = player_2pt_shots.filter(player2__isnull=False)
    player_3pt_shots = Event.objects.filter(possession__game__season__year=2019, event_type=1,
                                            zone__in=['Arc3', 'Corner3'], player1=player)
    player_3pt_shots_assisted = player_3pt_shots.filter(player2__isnull=False)
    player_name = PlayerSeason.objects.get(pk=player).person.name
    player_id = PlayerSeason.objects.get(pk=player).person.id
    player_entry = {
        'id': player_id,
        'name': player_name,
        '2_makes': len(player_2pt_shots),
        '2_ast_pct': len(player_2pt_shots_assisted) / len(player_2pt_shots) * 100,
        '3_makes': len(player_3pt_shots),
        '3_ast_pct': len(player_3pt_shots_assisted) / len(player_3pt_shots) * 100,
    }
    player_entry['diff'] = player_entry['3_ast_pct'] - player_entry['2_ast_pct']
    assisted_percentages.append(player_entry)

assisted_percentages = sorted(assisted_percentages, key=lambda k: k['diff'])
pd.DataFrame(assisted_percentages).to_csv('jump_shot_assisted_pct.csv')
