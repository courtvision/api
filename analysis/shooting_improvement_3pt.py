import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nba_data_api.settings')

import django

django.setup()

from app.models.PlayByPlay import Event, Possession
from app.models.Player import PlayerSeason
from django.db.models import Count
import pandas as pd

jump_shots_3pt_counts = {
    x['player1']: x['total'] for x in
    Event.objects
        .filter(event_type__in=[1, 2], zone__in=['Arc3', 'Corner3'],
                player1__season__season_type='Regular Season')
        .values('player1')
        .annotate(total=Count('player1'))
        .order_by('-total')
    if x['total'] > 75
}

year_to_year = []
for player_season_id in jump_shots_3pt_counts:
    player_season = PlayerSeason.objects.get(pk=player_season_id)
    try:
        previous_player_season = PlayerSeason.objects.get(person_id=player_season.person.id,
                                                          season__year=player_season.season.year - 1,
                                                          season__season_type='Regular Season')
    except PlayerSeason.DoesNotExist:
        continue

    try:
        jump_shots_3pt_counts[previous_player_season.id]
    except KeyError:
        continue

    year1_fg3a = Event.objects.filter(player1=previous_player_season, event_type__in=[1, 2],
                                      zone__in=['Arc3', 'Corner3'])
    year1_fg3m = year1_fg3a.filter(event_type=1)
    year2_fg3a = Event.objects.filter(player1=player_season, event_type__in=[1, 2],
                                      zone__in=['Arc3', 'Corner3'])
    year2_fg3m = year2_fg3a.filter(event_type=1)

    year1_poss = Possession.objects.filter(event__offensive_players=previous_player_season,
                                           event__event_type__in=[1, 2, 3, 5]).distinct().extra().count()
    year2_poss = Possession.objects.filter(event__offensive_players=player_season,
                                           event__event_type__in=[1, 2, 3, 5]).distinct().extra().count()
    if year1_poss > 3000 and year2_poss > 3000:
        entry = {
            'id': player_season.person.id,
            'name': player_season.person.name,
            'year1': previous_player_season.season.year,
            'year2': player_season.season.year,
            'year1_poss': year1_poss,
            'year2_poss': year2_poss,
            'year1_fg3a': jump_shots_3pt_counts[previous_player_season.id],
            'year1_fg3m': len(year1_fg3m),
            'year2_fg3a': jump_shots_3pt_counts[player_season_id],
            'year2_fg3m': len(year2_fg3m),
        }
        entry['poss_pct_diff'] = entry['year2_poss'] / entry['year1_poss'] * 100
        entry['year1_fg3_pct'] = entry['year1_fg3m'] / entry['year1_fg3a'] * 100
        entry['year2_fg3_pct'] = entry['year2_fg3m'] / entry['year2_fg3a'] * 100
        entry['fg3a_pct_change'] = entry['year2_fg3a'] / entry['year1_fg3a'] * 100
        entry['fg3m_pct_change'] = entry['year2_fg3m'] / entry['year1_fg3m'] * 100
        entry['fg3_pct_diff'] = entry['year2_fg3_pct'] - entry['year1_fg3_pct']
        year_to_year.append(entry)

year_to_year = sorted(year_to_year, key=lambda k: k['fg3m_pct_change'], reverse=True)


