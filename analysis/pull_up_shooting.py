from nba_api.stats.endpoints import leaguedashptstats

pull_up_data = leaguedashptstats.LeagueDashPtStats(pt_measure_type='PullUpShot',
                                                   player_or_team='Player').get_data_frames()[0]
pull_up_data = pull_up_data[['PLAYER_ID', 'PLAYER_NAME', 'PULL_UP_FG3A', 'PULL_UP_FG3_PCT']]
catch_and_shoot_data = leaguedashptstats.LeagueDashPtStats(pt_measure_type='CatchShoot',
                                                           player_or_team='Player').get_data_frames()[0]
catch_and_shoot_data = catch_and_shoot_data[['PLAYER_ID', 'CATCH_SHOOT_FG3A', 'CATCH_SHOOT_FG3_PCT']]

merged_data = pull_up_data.merge(catch_and_shoot_data, on='PLAYER_ID')
merged_data = merged_data[merged_data['PULL_UP_FG3A'] > 50]
merged_data = merged_data[merged_data['CATCH_SHOOT_FG3A'] > 50]
merged_data['PCT_PULL_UP'] = merged_data['PULL_UP_FG3A'] / (
            merged_data['PULL_UP_FG3A'] + merged_data['CATCH_SHOOT_FG3A'])
merged_data['PCT_DIFF'] = merged_data['CATCH_SHOOT_FG3_PCT'] - merged_data['PULL_UP_FG3_PCT']
merged_data = merged_data.sort_values(by='PCT_DIFF')

merged_data.to_csv('pull_up_shooting.csv')
