import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nba_data_api.settings')

import django

django.setup()

from app.models.PlayByPlay import Event, Possession
from app.models import PlayerSeason, TeamGame
from django.db.models import F, Count

self_oreb_counts = Event.objects.filter(event_type=4, shot_rebound_link__player1=F('player1'), possession__game__season__year=2019).values(
    'player1').annotate(total=Count('player1')).order_by('-total')

data = list()
for x in self_oreb_counts:
    if x['total'] >= 20:
        player = PlayerSeason.objects.get(id=x['player1'])
        player_missed_shots = Event.objects.filter(event_type=2, player1=player).extra().count()
        player_games = TeamGame.objects.filter(players=player).distinct().extra().count()
        player_possessions = Possession.objects.filter(event__offensive_players=player,
                                                       event__event_type__in=[1, 2, 3, 5]).distinct().extra().count()
        data.append({
            'player': f'{player.person.name} - {player.season.year} {player.season.season_type}',
            'games_played': player_games,
            'possessions_played': player_possessions,
            'self_orebs': x['total'],
            'self_orebs_per_game': round(x['total'] / player_games, 2),
            'self_orebs_per_100_possessions': round(x['total'] / player_possessions * 100, 2),
            'missed_shots': player_missed_shots,
            'pct_missed_self_orebed': round(x['total'] / player_missed_shots * 100, 2)
        })
data = sorted(data, key=lambda x: x['self_orebs_per_game'], reverse=True)
