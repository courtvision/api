import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nba_data_api.settings')

import django

django.setup()

from django.db.models import Count
from app.models import Event, PlayerSeason, Possession, TeamGame

assist_pairs = Event.objects.filter(event_type=1, player1__isnull=False, player2__isnull=False,
                                    description__contains='Alley Oop', possession__game__season__year=2019).values('player1', 'player2').annotate(
    total=Count('pk')).order_by('-total')

data = list()
for pair in assist_pairs:
    if pair['total'] >= 10:
        scorer = PlayerSeason.objects.get(pk=pair['player1'])
        passer = PlayerSeason.objects.get(pk=pair['player2'])
        player_games = TeamGame.objects.filter(players=scorer).filter(players=passer).distinct().extra().count()
        player_possessions = Possession.objects.filter(event__offensive_players=scorer).filter(
            event__offensive_players=passer, event__event_type__in=[1, 2, 3, 5]).distinct().extra().count()
        data.append({
            'scorer': f'{scorer.person.name}',
            'passer': f'{passer.person.name}',
            'season': f'{scorer.season.year} {scorer.season.season_type}',
            'games': player_games,
            'possessions': player_possessions,
            'assists': pair['total'],
            'assists_per_100_possessions': round(pair['total'] / player_possessions * 100, 2),
            'assists_per_game': round(pair['total'] / player_games, 2)
        })
data = sorted(data, key=lambda x: x['assists_per_game'], reverse=True)
2 + 2
