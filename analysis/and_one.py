import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nba_data_api.settings')

import django

django.setup()

from django.db.models import Count
from app.models import Event, PlayerSeason, TeamGame, Possession

and_one_counts = Event.objects.filter(foul_shot_link__isnull=False).values('player1').annotate(
    total=Count('player1')).order_by('-total')

data = list()
for x in and_one_counts:
    if x['total'] >= 5:
        player = PlayerSeason.objects.get(id=x['player1'])
        player_games = TeamGame.objects.filter(players=player).distinct().extra().count()
        player_shots = Event.objects.filter(event_type__in=[2, 3], player1=player).extra().count()
        player_possessions = Possession.objects.filter(event__offensive_players=player,
                                                       event__event_type__in=[1, 2, 3, 5]).distinct().extra().count()
        data.append({
            'player': f'{player.person.name} - {player.season.year} {player.season.season_type}',
            'games_played': player_games,
            'possessions_played': player_possessions,
            'and_ones': x['total'],
            'and_ones_per_game': round(x['total'] / player_games, 2),
            'and_ones_per_100_possessions': round(x['total'] / player_possessions * 100, 2),
            'and_ones_per_100_shots': round(x['total'] / player_shots * 100, 2)
        })

data = sorted(data, key=lambda x: x['and_ones_per_game'], reverse=True)
2 + 2
