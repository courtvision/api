from rest_framework.serializers import ModelSerializer

from app.models import Possession, Event
from app.models.Game import Game
from app.models.Game import TeamGame
from app.serializers.Player import PlayerSeasonModelSerializer
from app.serializers.Season import SeasonModelSerializer
from app.serializers.Team import TeamSeasonModelSerializer


class TeamGameModelSerializer(ModelSerializer):
    team = TeamSeasonModelSerializer()
    players = PlayerSeasonModelSerializer(many=True)

    class Meta:
        model = TeamGame
        fields = '__all__'


class GameEventModelSerializer(ModelSerializer):
    class Meta:
        model = Event
        fields = '__all__'


class GamePossessionModelSerializer(ModelSerializer):
    event_set = GameEventModelSerializer(many=True)

    class Meta:
        model = Possession
        fields = '__all__'


class GameModelSerializer(ModelSerializer):
    home_team_game = TeamGameModelSerializer()
    away_team_game = TeamGameModelSerializer()
    season = SeasonModelSerializer()
    possession_set = GamePossessionModelSerializer(many=True)

    class Meta:
        model = Game
        fields = '__all__'
