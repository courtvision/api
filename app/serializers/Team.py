from rest_framework.serializers import ModelSerializer

from app.models.Team import TeamSeason


class TeamSeasonModelSerializer(ModelSerializer):
    class Meta:
        model = TeamSeason
        fields = '__all__'
