from rest_framework.serializers import ModelSerializer

from app.models.PlayByPlay import Possession, Event
from app.serializers.Team import TeamSeasonModelSerializer


class EventModelSerializer(ModelSerializer):
    class Meta:
        model = Event
        fields = '__all__'


class PossessionModelSerializer(ModelSerializer):
    offensive_team = TeamSeasonModelSerializer()
    defensive_team = TeamSeasonModelSerializer()
    event_set = EventModelSerializer(many=True)

    class Meta:
        model = Possession
        fields = '__all__'
        depth = 1
