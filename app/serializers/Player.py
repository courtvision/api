from rest_framework.serializers import ModelSerializer

from app.models.Player import Person, PlayerSeason


class PersonModelSerializer(ModelSerializer):
    class Meta:
        model = Person
        fields = '__all__'


class PlayerSeasonModelSerializer(ModelSerializer):
    person = PersonModelSerializer()

    class Meta:
        model = PlayerSeason
        fields = '__all__'
