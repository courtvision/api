from rest_framework.serializers import ModelSerializer

from app.models.Season import Season


class SeasonModelSerializer(ModelSerializer):
    class Meta:
        model = Season
        fields = '__all__'
