import re


class Year:
    year_as_string: str

    def __init__(self, first_year: int):
        year_str = str(first_year)
        if re.match(r'[1-2][09][0-9]{2}', year_str):
            self.year_as_string = year_str + "-" + str(first_year + 1)[2:4]
        else:
            raise ValueError('Unexpected Year Format!')
