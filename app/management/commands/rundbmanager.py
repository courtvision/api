from datetime import datetime

import pbpstats
from apscheduler.schedulers.background import BackgroundScheduler
from django.core.management.commands.runserver import BaseRunserverCommand
from django_apscheduler.jobstores import DjangoJobStore, register_events, register_job

from app.database_manager import DatabaseManager


class Command(BaseRunserverCommand):
    help = 'Run the Database Manager to initialize and update Database'
    default_port = '8001'

    def handle(self, *args, **options):
        scheduler = BackgroundScheduler()
        scheduler.add_jobstore(DjangoJobStore(), "database_manager")

        pbpstats.DATA_DIRECTORY = '/home/patrick/pbpdata'

        @register_job(scheduler, "date", run_date=datetime.now())
        def initialize_data():
            DatabaseManager.initialize()

        register_events(scheduler)

        scheduler.start()

        super().handle(*args, **options)
