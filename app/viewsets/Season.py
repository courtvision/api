from rest_framework.viewsets import ReadOnlyModelViewSet

from app.models.Season import Season
from app.serializers.Season import SeasonModelSerializer


class SeasonModelViewSet(ReadOnlyModelViewSet):
    queryset = Season.objects.all()
    serializer_class = SeasonModelSerializer
