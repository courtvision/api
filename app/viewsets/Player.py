from rest_framework.viewsets import ReadOnlyModelViewSet

from app.models.Player import Person
from app.serializers.Player import PersonModelSerializer


class PlayerModelViewSet(ReadOnlyModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonModelSerializer
