from rest_framework.viewsets import ReadOnlyModelViewSet

from app.models.Game import Game
from app.serializers.Game import GameModelSerializer


class GameModelViewSet(ReadOnlyModelViewSet):
    queryset = Game.objects.all()
    serializer_class = GameModelSerializer
