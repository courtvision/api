from rest_framework.viewsets import ReadOnlyModelViewSet

from app.models.Team import TeamSeason
from app.serializers.Team import TeamSeasonModelSerializer


class TeamModelViewSet(ReadOnlyModelViewSet):
    queryset = TeamSeason.objects.all()
    serializer_class = TeamSeasonModelSerializer
