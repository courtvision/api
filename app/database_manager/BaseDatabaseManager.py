import logging


class BaseDatabaseManager:
    logger = logging.getLogger(__name__)

    def initialize_data(self) -> None:
        self.logger.info(f'-----{self.__class__.__name__} Initialize-----')

    def update_data(self) -> None:
        self.logger.info(f'-----{self.__class__.__name__} Update-----')
