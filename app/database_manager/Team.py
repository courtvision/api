from typing import List, Dict

from nba_api.stats.endpoints._base import Endpoint
from nba_api.stats.endpoints.leaguestandings import LeagueStandings
from nba_api.stats.endpoints.teamgamelogs import TeamGameLogs

from app.database_manager.SeasonBasedDatabaseManager import SeasonBasedBaseDatabaseManager
from app.models import Season, Franchise, TeamSeason


class TeamDatabaseManager(SeasonBasedBaseDatabaseManager):

    def update_data_for_year(self, season: Season) -> None:
        super().update_data_for_year(season)
        standings_endpoint: Endpoint = LeagueStandings(season=season.formatted_year())
        standings_data: List[Dict] = standings_endpoint.get_normalized_dict()['Standings']
        game_log_endpoint: Endpoint = TeamGameLogs(season_nullable=season.formatted_year(),
                                                   season_type_nullable=season.season_type)
        game_log_data: List[Dict] = game_log_endpoint.get_normalized_dict()['TeamGameLogs']
        id_abb_map: Dict = {
            x['TeamID']: next(
                (team['TEAM_ABBREVIATION'] for team in game_log_data if team['TEAM_ID'] == x['TeamID']), None)
            for x in standings_data
        }
        for standings_team in standings_data:
            franchise_query = Franchise.objects.filter(pk=standings_team['TeamID'])
            if not franchise_query.exists():
                f: Franchise = Franchise(id=standings_team['TeamID'])
                self.logger.info(f'Adding {f.id}')
                f.save()
            else:
                f: Franchise = franchise_query.first()
            if not TeamSeason.objects.filter(franchise=f, season=season).exists() \
                    and id_abb_map[standings_team['TeamID']] is not None:
                t: TeamSeason = TeamSeason(
                    season=season,
                    city=standings_team['TeamCity'],
                    name=standings_team['TeamName'],
                    abbreviation=id_abb_map[standings_team['TeamID']],
                    conference=standings_team['Conference'],
                    division=standings_team['Division'],
                    franchise=f
                )
                self.logger.info(f'Adding {t.city} {t.name} {t.season.year} {t.season.season_type}')
                t.save()
