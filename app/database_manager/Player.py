from typing import List, Dict

from nba_api.stats.endpoints import leaguedashplayerbiostats
from nba_api.stats.endpoints._base import Endpoint

from app.database_manager.SeasonBasedDatabaseManager import SeasonBasedBaseDatabaseManager
from app.models import Season, Person, PlayerSeason


class PlayerDatabaseManager(SeasonBasedBaseDatabaseManager):

    def update_data_for_year(self, season: Season) -> None:
        super().update_data_for_year(season)
        endpoint: Endpoint = leaguedashplayerbiostats.LeagueDashPlayerBioStats(season=season.formatted_year(),
                                                                               season_type_all_star=season.season_type)
        data: List[Dict] = endpoint.get_normalized_dict()['LeagueDashPlayerBioStats']
        for d in data:
            person = Person.objects.filter(pk=d['PLAYER_ID'])
            if not person.exists():
                p: Person = Person(
                    id=d['PLAYER_ID'],
                    name=d['PLAYER_NAME'],
                    college=d['COLLEGE'] if d['COLLEGE'] != 'None' else None,
                    country=d['COUNTRY'] if str(d['COUNTRY']).strip() != '' else None,
                    draft_round=d['DRAFT_ROUND'] if d['DRAFT_ROUND'] != 'Undrafted' else None,
                    draft_number=d['DRAFT_NUMBER'] if d['DRAFT_ROUND'] != 'Undrafted' else None
                )
                self.logger.info(f"Adding Person {p.name}")
                p.save()
            else:
                p: Person = person.get()
            if not PlayerSeason.objects.filter(person=p, season=season):
                ps: PlayerSeason = PlayerSeason(
                    person=p,
                    season=season,
                    age=d['AGE'],
                    height=d['PLAYER_HEIGHT_INCHES'],
                    weight=d['PLAYER_WEIGHT'] if (d['PLAYER_WEIGHT'] and d['PLAYER_WEIGHT'].isdigit()) else None
                )
                self.logger.info(f"Adding Player Season for {p.name} age {ps.age}")
                ps.save()
