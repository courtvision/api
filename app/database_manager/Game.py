from typing import Dict, List

from nba_api.stats.endpoints._base import Endpoint
from nba_api.stats.endpoints.teamgamelogs import TeamGameLogs

from app.database_manager.SeasonBasedDatabaseManager import SeasonBasedBaseDatabaseManager
from app.models.Game import TeamGame, Game
from app.models.Season import Season
from app.models.Team import TeamSeason


class GameDatabaseManager(SeasonBasedBaseDatabaseManager):

    def update_data_for_year(self, season: Season) -> None:
        super().update_data_for_year(season)
        game_log_endpoint: Endpoint = TeamGameLogs(season_nullable=season.formatted_year(),
                                                   season_type_nullable=season.season_type)
        game_log_data: List[Dict] = game_log_endpoint.get_normalized_dict()['TeamGameLogs']
        game_ids = list(set(map(lambda x: x['GAME_ID'], game_log_data)))
        for game_id in game_ids:
            team_game_entries = [game for game in game_log_data if game['GAME_ID'] == game_id]
            home_team_game_entry = [game for game in team_game_entries if '@' in game['MATCHUP']]
            away_team_game_entry = [game for game in team_game_entries if '@' not in game['MATCHUP']]
            if len(home_team_game_entry) == 1 and len(away_team_game_entry) == 1:
                if not Game.objects.filter(pk=game_id).exists():
                    home_team_game_entry = home_team_game_entry[0]
                    away_team_game_entry = away_team_game_entry[0]
                    home_team_game = TeamGame(
                        team=TeamSeason.objects.filter(franchise__id=home_team_game_entry['TEAM_ID'], season=season).first(),
                        score=home_team_game_entry['PTS'],
                        win=home_team_game_entry['WL'] == 'W'
                    )
                    away_team_game = TeamGame(
                        team=TeamSeason.objects.filter(franchise__id=away_team_game_entry['TEAM_ID'], season=season).first(),
                        score=away_team_game_entry['PTS'],
                        win=away_team_game_entry['WL'] == 'W'
                    )
                    game = Game(
                        id=game_id,
                        season=season,
                        date=home_team_game_entry['GAME_DATE'].split('T')[0],
                        home_team_game=home_team_game,
                        away_team_game=away_team_game
                    )
                    self.logger.info(f'Adding {away_team_game.team.name} @ {home_team_game.team.name} : {game.date}')
                    home_team_game.save()
                    away_team_game.save()
                    game.save()
