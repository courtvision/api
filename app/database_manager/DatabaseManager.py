from app.database_manager.Season import SeasonDatabaseManager
from app.database_manager.Player import PlayerDatabaseManager
from app.database_manager.Team import TeamDatabaseManager
from app.database_manager.Game import GameDatabaseManager
from app.database_manager.PlayByPlay import PlayByPlayDatabaseManager


class DatabaseManager:

    @staticmethod
    def initialize() -> None:
        SeasonDatabaseManager().initialize_data()
        PlayerDatabaseManager().initialize_data()
        TeamDatabaseManager().initialize_data()
        GameDatabaseManager().initialize_data()
        PlayByPlayDatabaseManager().initialize_data()

    @staticmethod
    def update() -> None:
        SeasonDatabaseManager().update_data()
        PlayerDatabaseManager().update_data()
        TeamDatabaseManager().update_data()
        GameDatabaseManager().update_data()
        PlayByPlayDatabaseManager().update_data()