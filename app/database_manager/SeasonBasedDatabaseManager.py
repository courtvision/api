from app.database_manager.BaseDatabaseManager import BaseDatabaseManager
from app.models import Season


class SeasonBasedBaseDatabaseManager(BaseDatabaseManager):

    def update_data_for_year(self, season: Season) -> None:
        self.logger.info(f'-----{self.__class__.__name__} {season.year} {season.season_type}-----')

    def initialize_data(self) -> None:
        super().initialize_data()
        for y in Season.objects.all():
            self.update_data_for_year(y)

    def update_data(self) -> None:
        super().update_data()
        self.update_data_for_year(Season.objects.first(is_current=True))
