from django.db import transaction
from pbpstats.data_game_data import DataGameData
from pbpstats.game_data import GameData
from pbpstats.game_data import InvalidNumberOfStartersException
from pbpstats.stats_game_data import StatsGameData

from app.database_manager.SeasonBasedDatabaseManager import SeasonBasedBaseDatabaseManager
from app.models import Season, Possession, Game, TeamSeason, Event, Person, PlayerSeason


class PlayByPlayDatabaseManager(SeasonBasedBaseDatabaseManager):

    @staticmethod
    def convert_clock_time(clock_time: str, period_number: int) -> float:
        [minutes, seconds] = clock_time.split(':')
        if period_number <= 4:
            period_end_time = 720 * period_number
        else:
            period_end_time = 2880 + ((period_number - 4) * 300)
        return period_end_time - (int(minutes) * 60 + float(seconds))

    @transaction.atomic
    def save_game_data(self, season: Season, game_data: GameData):
        game = Game.objects.get(pk=int(game_data.GameId))
        self.logger.info(f'Adding {game.id}')

        home_team = TeamSeason.objects.get(franchise__id=int(game_data.HomeTeamId), season=season)

        game.home_team_game.players.set([PlayerSeason.objects.get(person_id=player_id, season=season) for player_id in
                                         game_data.Players[game_data.HomeTeamId]])
        game.away_team_game.players.set([PlayerSeason.objects.get(person_id=player_id, season=season) for player_id in
                                         game_data.Players[game_data.VisitorTeamId]])

        game.save()

        possessions = list()
        events = list()
        for period in game_data.Periods:
            previous_event = None
            for possession in period.Possessions:
                p = Possession(
                    game=game,
                    defensive_team=TeamSeason.objects.get(franchise__id=int(possession.DefenseTeamId),
                                                          season=season) if possession.DefenseTeamId is not None else None,
                    offensive_team=TeamSeason.objects.get(franchise__id=int(possession.OffenseTeamId),
                                                          season=season) if possession.OffenseTeamId is not None else None
                )
                p.save()
                possessions.append(p)
                last_oreb = None

                offense_is_home = p.offensive_team is home_team

                for event in possession.Events:
                    offensive_player_ids = event.current_players[possession.OffenseTeamId]
                    defensive_player_ids = event.current_players[possession.DefenseTeamId]
                    try:
                        team = TeamSeason.objects.get(franchise__id=int(event.team_id), season=season) if (
                                event.team_id is not None and event.team_id != '0' and event.team_id != '') else None
                    except TeamSeason.DoesNotExist:
                        team = None

                    try:
                        player1 = PlayerSeason.objects.get(person_id=int(event.player_id), season=season) if (
                                event.player_id is not None and event.player_id != '0') else None
                    except PlayerSeason.DoesNotExist:
                        player1 = None

                    try:
                        player2 = PlayerSeason.objects.get(person_id=int(event.player2_id), season=season) if (
                                event.player2_id is not None and event.player2_id != '') else None
                    except PlayerSeason.DoesNotExist:
                        player2 = None

                    try:
                        player3 = PlayerSeason.objects.get(person_id=int(event.player3_id), season=season) if (
                                event.player3_id is not None and event.player3_id != '') else None
                    except PlayerSeason.DoesNotExist:
                        player3 = None

                    e = Event(
                        time=self.convert_clock_time(event.clock_time, period.Number),
                        description=event.description,
                        event_type=event.etype,
                        offense_score=event.home_score if offense_is_home else event.visitor_score,
                        defense_score=event.visitor_score if offense_is_home else event.home_score,
                        loc_x=event.loc_x,
                        loc_y=event.loc_y,
                        mtype=event.mtype,
                        number=event.number,
                        order=event.order,
                        player1=player1,
                        player2=player2,
                        player3=player3,
                        team=team,
                        possession=p,
                        previous_offensive_rebound=last_oreb,
                        zone=event.get_shot_type(),
                        foul_type=event.get_foul_type()
                    )
                    p.save()
                    e.save()
                    e.offensive_players.set(
                        [PlayerSeason.objects.get(person_id=player_id, season=season) for player_id in
                         offensive_player_ids])
                    e.defensive_players.set(
                        [PlayerSeason.objects.get(person_id=player_id, season=season) for player_id in
                         defensive_player_ids])
                    e.save()
                    events.append(e)

                    if previous_event is not None:
                        previous_event.next_event = e
                        previous_event.save()
                        e.previous_event = previous_event
                        e.save()

                    if e.event_type == 4:
                        reb_data = event.get_rebound_data()
                        if reb_data is not None and reb_data['rebounded_shot'] is not None:
                            try:
                                shot = Event.objects.get(possession__game=game,
                                                         number=reb_data['rebounded_shot'].number)
                            except Event.DoesNotExist:
                                shot = None
                            if shot is not None:
                                e.shot_rebound_link = shot
                                e.save()
                                shot.shot_rebound_link = e
                                shot.save()

                                if shot.team == e.team:
                                    last_oreb = e

                    if e.event_type == 3:
                        foul = event.get_foul_that_resulted_in_ft()
                        try:
                            foul_event = Event.objects.get(possession=p, number=foul.number)
                        except (Event.DoesNotExist, AttributeError):
                            foul_event = None
                        if foul_event is not None:
                            foul_event.free_throw_foul_link.add(e)
                            foul_event.save()

                    if e.event_type == 6:
                        and1_shot = event.get_and1_shot()
                        if and1_shot is not None:
                            try:
                                and1_shot_event = Event.objects.get(possession=p, number=and1_shot.number)
                                e.shot_foul_link = and1_shot_event
                                e.save()
                            except Event.DoesNotExist:
                                None

                    previous_event = e

        for ix, p in enumerate(possessions):
            if ix != 0:
                p.previous_possession = possessions[ix - 1]
            if ix != (len(possessions) - 1):
                p.next_possession = possessions[ix + 1]
            p.save()

        for ix, e in enumerate(events):
            if ix != 0:
                e.previous_event = events[ix - 1]
            if ix != (len(events) - 1):
                e.next_event = events[ix + 1]
            e.save()

    def update_data_for_year(self, season: Season) -> None:
        games = Game.objects.filter(season=season)
        game_ids = list(set([g.formatted_id_for_api() for g in games]))
        game_ids.sort()
        game_ids.reverse()

        for game_id in game_ids:
            if not Possession.objects.filter(game_id=int(game_id)).exists():
                try:
                    if season.year >= 2016:
                        game_data = DataGameData(game_id)
                    else:
                        game_data = StatsGameData(game_id)
                    game_data.get_game_data(ignore_rebound_and_shot_order=True,
                                            ignore_back_to_back_possessions=True)
                    self.save_game_data(season, game_data)
                except InvalidNumberOfStartersException:
                    self.logger.error(f'InvalidNumberOfStartersException - {game_id}')
                except Exception as e:
                    self.logger.error(f'{e} - {game_id}')
