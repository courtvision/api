from typing import List, Dict

from nba_api.stats.endpoints import TeamGameLogs
from nba_api.stats.endpoints._base import Endpoint
from nba_api.stats.library.parameters import SeasonYear, SeasonTypePlayoffs

from app.database_manager.BaseDatabaseManager import BaseDatabaseManager
from app.models import Season
from app.util.Formatters import Year


class SeasonDatabaseManager(BaseDatabaseManager):
    current_year = SeasonYear.current_season_year
    years = range(1996, current_year + 1)
    season_types = [SeasonTypePlayoffs.regular, SeasonTypePlayoffs.playoffs]

    def initialize_data(self) -> None:
        super().initialize_data()

        found_current = Season.objects.filter(is_current=True).exists()

        year_list = list(self.years)
        year_list.reverse()
        season_type_list = self.season_types
        season_type_list.reverse()

        for year in year_list:
            for season_type in season_type_list:
                if not Season.objects.filter(year=year, season_type=season_type).exists():
                    self.logger.info(f'-----{year} {season_type}-----')
                    if not found_current:
                        game_log_endpoint: Endpoint = TeamGameLogs(season_nullable=Year(year).year_as_string,
                                                                   season_type_nullable=season_type)
                        game_log_data: List[Dict] = game_log_endpoint.get_normalized_dict()['TeamGameLogs']
                        if len(game_log_data) > 0:
                            s: Season = Season(
                                year=year,
                                season_type=season_type,
                                is_current=True
                            )
                            s.save()
                            found_current = True
                    else:
                        s: Season = Season(
                            year=year,
                            season_type=season_type,
                            is_current=False
                        )
                        s.save()

    def update_data(self) -> None:
        super().update_data()

        year_list = list(self.years)
        season_type_list = self.season_types

        for year in year_list:
            for season_type in season_type_list:
                if not Season.objects.filter(year=year, season_type=season_type).exists():
                    self.logger.info(f'-----{year} {season_type}-----')
                    game_log_endpoint: Endpoint = TeamGameLogs(season_nullable=Year(year).year_as_string,
                                                               season_type_nullable=season_type)
                    game_log_data: List[Dict] = game_log_endpoint.get_normalized_dict()['TeamGameLogs']
                    if len(game_log_data) > 0:
                        s: Season = Season(
                            year=year,
                            season_type=season_type,
                            is_current=True
                        )
                        s.save()
                        previous_current: Season = Season.objects.first(is_current=True)
                        previous_current.is_current = False
                        previous_current.save()
