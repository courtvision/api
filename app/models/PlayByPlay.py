from django.db import models

from app.models import TeamSeason, Game, PlayerSeason


class Possession(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    defensive_team = models.ForeignKey(TeamSeason, related_name='defensive_team', on_delete=models.CASCADE)
    offensive_team = models.ForeignKey(TeamSeason, related_name='offensive_team', on_delete=models.CASCADE)
    next_possession = models.ForeignKey('self', related_name='next', on_delete=models.CASCADE, null=True)
    previous_possession = models.ForeignKey('self', related_name='previous', on_delete=models.CASCADE,
                                            null=True)


class Event(models.Model):
    time = models.FloatField()
    description = models.CharField(max_length=200)
    event_type = models.IntegerField()
    offense_score = models.IntegerField()
    defense_score = models.IntegerField()
    loc_x = models.IntegerField(null=True)
    loc_y = models.IntegerField(null=True)
    mtype = models.IntegerField()
    previous_event = models.ForeignKey('self', related_name='previous', on_delete=models.CASCADE, null=True)
    next_event = models.ForeignKey('self', related_name='next', on_delete=models.CASCADE, null=True)
    number = models.IntegerField()
    order = models.IntegerField()
    player1 = models.ForeignKey(PlayerSeason, related_name='events_as_player1', on_delete=models.CASCADE, null=True)
    player2 = models.ForeignKey(PlayerSeason, related_name='events_as_player2', on_delete=models.CASCADE, null=True)
    player3 = models.ForeignKey(PlayerSeason, related_name='events_as_player3', on_delete=models.CASCADE, null=True)
    team = models.ForeignKey(TeamSeason, on_delete=models.CASCADE, null=True)
    possession = models.ForeignKey(Possession, on_delete=models.CASCADE)
    offensive_players = models.ManyToManyField(PlayerSeason, related_name='events_in_offensive_players')
    defensive_players = models.ManyToManyField(PlayerSeason, related_name='events_in_defensive_players')
    previous_offensive_rebound = models.ForeignKey('self', related_name='shots_after_offensive_rebound',
                                                   on_delete=models.CASCADE, null=True)
    shot_rebound_link = models.ForeignKey('self', related_name='rebound_shot_link', on_delete=models.CASCADE, null=True)
    free_throw_foul_link = models.ManyToManyField('self', related_name='foul_free_throw_link')
    shot_foul_link = models.ForeignKey('self', related_name='foul_shot_link', on_delete=models.CASCADE, null=True)
    zone = models.CharField(max_length=20, null=True)
    foul_type = models.CharField(max_length=50, null=True)
