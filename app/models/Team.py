from django.db import models

from app.models import Season, PlayerSeason


class Franchise(models.Model):
    id = models.BigIntegerField(primary_key=True)


class TeamSeason(models.Model):
    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    city = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    abbreviation = models.CharField(max_length=50)
    conference = models.CharField(max_length=10)
    division = models.CharField(max_length=20)
    franchise = models.ForeignKey(Franchise, on_delete=models.CASCADE)
    players = models.ManyToManyField(PlayerSeason)
