from django.db import models


class Season(models.Model):
    year = models.IntegerField()
    season_type = models.CharField(max_length=50)
    is_current = models.BooleanField()

    def formatted_year(self):
        return str(self.year) + '-' + str(self.year + 1)[2:4]
