from django.db import models

from app.models import Season


class Person(models.Model):
    id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=50, null=True)
    college = models.CharField(max_length=50, null=True)
    country = models.CharField(max_length=50, null=True)
    draft_round = models.IntegerField(null=True)
    draft_number = models.IntegerField(null=True)


class PlayerSeason(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    age = models.IntegerField(null=True)
    height = models.IntegerField(null=True)
    weight = models.IntegerField(null=True)
