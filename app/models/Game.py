from django.db import models

from app.models import TeamSeason, Season, PlayerSeason


class TeamGame(models.Model):
    team = models.ForeignKey(TeamSeason, on_delete=models.CASCADE, related_name='team')
    score = models.IntegerField()
    win = models.BooleanField()
    players = models.ManyToManyField(PlayerSeason)


class Game(models.Model):
    id = models.BigIntegerField(primary_key=True)
    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    date = models.DateField()
    home_team_game = models.ForeignKey(TeamGame, on_delete=models.CASCADE, related_name='home_team')
    away_team_game = models.ForeignKey(TeamGame, on_delete=models.CASCADE, related_name='away_team')

    def formatted_id_for_api(self) -> str:
        return f'{self.id:010}'
