# Generated by Django 3.0.5 on 2020-05-09 16:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0016_teamgame_players'),
    ]

    operations = [
        migrations.RenameField(
            model_name='game',
            old_name='away_team',
            new_name='away_team_game',
        ),
        migrations.RenameField(
            model_name='game',
            old_name='home_team',
            new_name='home_team_game',
        ),
    ]
