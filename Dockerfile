FROM python:3.7 as base
RUN pip install pipenv

FROM base as packages
WORKDIR /app/
COPY Pipfile* /app/
RUN pipenv install --system --deploy --ignore-pipfile

FROM packages as app
WORKDIR /app/
COPY manage.py /app/
COPY ./app /app/app/
COPY ./nba_data_api /app/nba_data_api/
